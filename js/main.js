class Product
{
    constructor(id, name, categoryId, saleDate, qulity, isDelete) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.qulity = qulity;
        this.isDelete = isDelete;
    }
}

function initListProduct() {
        let product1 = new Product(1, 'Iphone 1 ', 1, new Date(2019, 11, 24, 10, 33, 30), 0, true);
        listProduct.push(product1);
        let product2 = new Product(2, 'Iphone 2 ', 1, new Date(2019, 11, 24, 10, 33, 30), 15, true);
        listProduct.push(product2);
        let product3 = new Product(3, 'Iphone 3 ', 1, new Date(2020, 11, 24, 10, 33, 30), 5, false);
        listProduct.push(product3);
        let product4 = new Product(4, 'Iphone 4', 1, new Date(2022, 11, 24, 10, 33, 30), 8, false);
        listProduct.push(product4);
        let product5 = new Product(5, 'Iphone 5 ', 1, new Date(2022, 11, 24, 10, 33, 30), 0, true);
        listProduct.push(product5);
        let product6 = new Product(6, 'SamSung 6', 2, new Date(2019, 11, 24, 10, 33, 30), 4, false);
        listProduct.push(product6);
        let product7 = new Product(7, 'SamSung 7', 2, new Date(2019, 11, 24, 10, 33, 30), 2, false);
        listProduct.push(product7);
        let product8 = new Product(8, 'SamSung 8', 2, new Date(2022, 11, 24, 10, 33, 30), 0, true);
        listProduct.push(product8);
        let product9 = new Product(9, 'SamSung 9', 2, new Date(2021, 11, 24, 10, 33, 30), 9, false);
        listProduct.push(product9);
        let product10 = new Product(10, 'SamSung 10', 2, new Date(2021, 11, 24, 10, 33, 30), 1, true);
        listProduct.push(product10);
}

const listProduct = [];
initListProduct();
console.log(listProduct);

//Bài 11
function filterProductByIdUseFor(listProduct, idProduct) {
    for(let product of listProduct)
    {
        if(product.id === idProduct)
        {
            return product.name;
        }
    }
    return null;
}

function filterProductById(listProduct, idProduct) {
    productById = listProduct.find(product => product.id === idProduct);
    return productById.name;
}

// const nameProduct = filterProductByIdUseFor(listProduct, 5);
// const nameProduct = filterProductById(listProduct, 8);

// console.log('Get name Product by ID: ', nameProduct);

//Bài 12
function filterProductByQulityUseFor(listProduct) {

    let listProductByQulity = [];

    for(let product of listProduct)
    {
        if(product.qulity > 0 && product.isDelete === false)
        {
            listProductByQulity.push(product);
        }
    }
    return listProductByQulity;
}

function filterProductByQulity(listProduct)  {

    let listProductByQulity = listProduct.filter(product => product.qulity > 0 && product.isDelete === false);
    return listProductByQulity;
}

// const listProductByQulity = filterProductByQulityUseFor(listProduct);
// const listProductByQulity = filterProductByQulity(listProduct);

// console.log('Array Product có qulity>0 và chưa bị xóa : ', listProductByQulity);

//Bài 13

function filterProductBySaleDateUseFor(listProduct) {

    let listNameProductBySaleDate = [];

    for(let product of listProduct)
    {
        if(product.saleDate > Date.now() && product.isDelete === false)
        {
            listNameProductBySaleDate.push(product.name);
        }
    }
    return listNameProductBySaleDate;
}

function filterProductBySaleDate(listProduct) {

    let listProductBySaleDate = listProduct.filter(product => product.saleDate > Date.now() && product.isDelete === false);
    let listNameProductBySaleDate = listProductBySaleDate.map(productBySaleDate => productBySaleDate.name);

    return listNameProductBySaleDate;
}

// const listNameProductBySaleDate = filterProductBySaleDateUseFor(listProduct);
// const listNameProductBySaleDate = filterProductBySaleDate(listProduct);

// console.log('List Name Product By Sale Date : ', listNameProductBySaleDate);

//Bài 14
function totalProduct(listProduct) {

    let listProductByQulity = listProduct.filter(product => product.qulity > 0 && product.isDelete === false);
    let totalProductByQulity = listProductByQulity.reduce((total, productByQulity) => total += productByQulity.qulity, 0);

    return totalProductByQulity;
}

function totalProductNoReduce(listProduct) {

    let totalProductByQulity = 0;
    let listProductByQulity = listProduct.filter(product => product.qulity > 0 && product.isDelete === false);

    for(let product of listProductByQulity)
    {
        if(product.qulity > 0 && product.isDelete === false)
        {
            totalProductByQulity += product.qulity;
        }
    }
    return totalProductByQulity;
}

// const totalProductByQulity = totalProduct(listProduct);
// const totalProductByQulity = totalProductNoReduce(listProduct);

// console.log('Tổng số product(tổng qulity) chưa bị xóa : ', totalProductByQulity);

//Bài 15
function isHaveProductInCategory(listProduct, categoryId) {
    let productInCategory = listProduct.find(product => product.categoryId === categoryId);

    if(productInCategory)
    {
        return true;
    }

    return false;
}

function isHaveProductInCategoryUseFor(listProduct, categoryId) {
    for(let product of listProduct)
    {
        if(product.categoryId === categoryId)
        {
            return true;
        }
    }
    return false;
}

// const checkProductInCategory = isHaveProductInCategory(listProduct, 2);
// const checkProductInCategory = isHaveProductInCategoryUseFor(listProduct, 3);

// console.log('Có Product trong Category : ', checkProductInCategory);

// Bài 16
function filterProductBySaleDateUseFor(listProduct) {

    let listProductBySaleDate = [];
     
    for(let product of listProduct) {
        let listIdNameProduct = []
        if(product.saleDate > Date.now() && product.qulity > 0){
            listIdNameProduct.push(product.id.toString());
            listIdNameProduct.push(product.name);

            listProductBySaleDate.push(listIdNameProduct);
        }
    }
    return listProductBySaleDate;
}

function filterProductBySaleDate(listProduct) {
    // ProductBySaleDate = listProduct.filter(product => product.saleDate());
}

const listProductBySaleDate = filterProductBySaleDateUseFor(listProduct);

console.log('Product có saleDate > now and qulity > 0 : ', listProductBySaleDate);
